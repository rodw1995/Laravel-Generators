<?php

return [

    'paths' => [
        'routes' => app_path('Http/routes.php'),
        'controllers' => app_path('Http/Controllers'),
        'requests' => app_path('Http/Requests'),
        'controller_tests' => base_path('tests/Controllers'),
        'model_factories' => database_path('factories'),
    ],

    'templates' => [
        'entity_controller' => base_path('templates/entity_controller.template'),
        'add_route' => base_path('templates/add_route.template'),
        'create_validation_request' => base_path('templates/create_validation_request.template'),
        'update_validation_request' => base_path('templates/update_validation_request.template'),
        'has_one_relationship' => base_path('templates/Relationships/has_one_relationship.template'),
        'has_many_relationship' => base_path('templates/Relationships/has_many_relationship.template'),
        'belongs_to_relationship' => base_path('templates/Relationships/belongs_to_relationship.template'),
        'belongs_to_many_relationship' => base_path('templates/Relationships/belongs_to_many_relationship.template'),
        'add_to_fillable' => base_path('templates/add_to_fillable.template'),
        'create_fillable' => base_path('templates/create_fillable.template'),
        'controller_test' => base_path('templates/Tests/controller_test.template'),
        'model_factory' => base_path('templates/Tests/model_factory.template'),
        'add_foreign_key_to_model_factory' => base_path('templates/Tests/add_foreign_key_to_model_factory.template'),
    ],

    'events' => [
        'Rodw\LaravelGenerators\Events\EntityWasCreated' => [
            'Rodw\LaravelGenerators\Listeners\CreateEntityControllerAndRoutes',
            'Rodw\LaravelGenerators\Listeners\SetRelationshipsInModels',
            'Rodw\LaravelGenerators\Listeners\SetPropertiesAsFillableInModel',
        ],
    ],
];