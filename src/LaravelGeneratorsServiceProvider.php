<?php

namespace Rodw\LaravelGenerators;


use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;

class LaravelGeneratorsServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     * @param Dispatcher $dispatcher
     */
    public function boot(Dispatcher $dispatcher)
    {
        $this->publishes([
            __DIR__ . '/Config/rodw_laravel_generators.php' => config_path('rodw_laravel_generators.php'),
            __DIR__ . '/Templates' => base_path('templates'),
        ]);

        $eventsAndListeners = config('rodw_laravel_generators.events');

        foreach ((array) $eventsAndListeners as $event => $listeners) {
            foreach ((array) $listeners as $listener) {
                $dispatcher->listen($event, $listener);
            }
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->registerMakeEntityCommand();

        $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
    }

    private function registerBindings()
    {
        $this->app->bind('Rodw\FileManipulator\FileManipulatorInterface', 'Rodw\FileManipulator\FileManipulator');

        $this->app->bind('Rodw\TemplateParser\ClassLoader\ClassLoaderInterface', 'Rodw\TemplateParser\ClassLoader\ComposerClassLoaderAdapter');
        $this->app->bind('Rodw\TemplateParser\TemplateParserInterface', 'Rodw\TemplateParser\TemplateParser');

        $this->app->bind('Rodw\Generator\GeneratorInterface', 'Rodw\Generator\Generator');

        $this->app->bind('Rodw\FileManipulator\Readers\ClassReaderInterface', 'Rodw\FileManipulator\Readers\ClassReader');
        $this->app->bind('Rodw\FileManipulator\Readers\FileReaderInterface', 'Rodw\FileManipulator\Readers\FileReader');
    }

    private function registerMakeEntityCommand()
    {
        $this->app->singleton('command.make.entity', function ($app) {
            return $app['Rodw\LaravelGenerators\MakeEntity'];
        });

        $this->commands('command.make.entity');
    }
}