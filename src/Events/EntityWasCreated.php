<?php

namespace Rodw\LaravelGenerators\Events;


class EntityWasCreated
{
    /**
     * @var
     */
    private $entityClass;
    /**
     * @var
     */
    private $properties;
    /**
     * @var array
     */
    private $relationships;

    /**
     * Create a new event instance.
     *
     * @param String $entityClass
     * @param array $properties
     * @param array $relationships with the keys: entity, related_entity, type, reversed_type
     */
    public function __construct($entityClass, array $properties, array $relationships)
    {
        $this->entityClass = $entityClass;
        $this->properties = $properties;
        $this->relationships = $relationships;
    }

    /**
     * @return String
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return class_basename($this->getEntityClass());
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @return array
     */
    public function getRelationships()
    {
        return $this->relationships;
    }
}