<?php

namespace Rodw\LaravelGenerators;


use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Str;
use Laracasts\Generators\Migrations\SchemaParser;
use Rodw\LaravelGenerators\Events\EntityWasCreated;

class MakeEntity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rodw:make:entity {name : The name of the entity} {schema* : The properties for the entity} {--migrate : Whether artisan migrate should be called}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate everything for a new entity';

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * Create a new command instance.
     *
     * @param Dispatcher $dispatcher
     */
    public function __construct(Dispatcher $dispatcher)
    {
        parent::__construct();
        $this->dispatcher = $dispatcher;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $entityClass = $this->parseName($this->argument('name'));
        $entityName = class_basename($entityClass);

        // Check if the entity already exists
        if ($this->checkIfEntityExists($entityClass)) {
            $this->error('The entity "' . $entityClass . '" already exists!');
            return;
        }

        // Call the artisan make:model
        $this->call('make:model', [
            'name' => $entityClass,
        ]);

        $relationships = $this->askForRelationships($entityClass);

        $relationshipMigrations = [];
        $pivotMigrations = [];

        $table = Str::plural(Str::snake($entityName));
        $schema = $this->argument('schema');

        // Add the relationships to the schema
        foreach ($relationships as $relationship) {
            $relatedEntityName = class_basename($relationship['related_entity']);
            if ($relationship['type'] == '1' OR ($relationship['type'] == '0|1' AND $relationship['reversed_type'] != '1')) {
                $foreignKey = Str::snake($relatedEntityName) . '_id';
                $schema[] = $foreignKey . ':integer:foreign';
            } elseif ($relationship['reversed_type'] == '1' OR $relationship['reversed_type'] == '0|1') {
                // Add new migration for the foreign key
                $foreignKey = Str::snake($entityName) . '_id';

                $relationshipMigrations[] = [
                    'name'     => 'add_' . $table . '_table',
                    '--schema' => $foreignKey . ':integer:foreign',
                    '--model'  => false,
                ];
            } else {
                // Create a new migration for the pivot table of the many_to_many relationship
                $pivotMigrations[] = [
                    'tableOne' => $table,
                    'tableTwo' => Str::plural(Str::snake($relatedEntityName)),
                ];
            }
        }

        $properties = implode(',', $schema);

        // Create migration
        $this->call('make:migration:schema', [
            'name'     => 'create_' . $table . '_table',
            '--schema' => $properties,
            '--model'  => false,
        ]);

        // Call migrations for relationships
        foreach ($relationshipMigrations as $relationshipMigration) {
            $this->call('make:migration:schema', $relationshipMigration);
        }

        // And for the pivot tables
        foreach ($pivotMigrations as $pivotMigration) {
            $this->call('make:migration:pivot', $pivotMigration);
        }

        if ($this->option('migrate')) {
            // Execute migration
            $this->call('migrate');
        }

        $properties = (new SchemaParser)->parse($properties);

        // Fire the EntityWasCreated
        $results = $this->dispatcher->fire(new EntityWasCreated($entityClass, $this->filterProperties($properties), $relationships));

        // Listeners can return an anonymous as callback
        foreach ($results as $callback) {
            if (is_callable($callback)) {
                $callback($this);
            }
        }
    }

    private function checkIfEntityExists($entityClass)
    {
        return class_exists($entityClass);
    }

    private function askForRelationships($entityClass)
    {
        $relationships = [];

        $question = 'Do you want to add a relationship? [y|N]';

        // Ask for relationships
        while (true) {
            if ($this->confirm($question, false)) {
                $relatedModel = $this->parseName($this->ask('What is the name of the associated model?'));

                // Check if this model exists
                if ($this->checkIfEntityExists($relatedModel)) {
                    $relationOptions = ['0|1', '1', '0|*', '1|*'];
                    $typeOfRelationship = $this->choice(class_basename($entityClass) . ' .... '
                        . class_basename($relatedModel) . '(s)',
                        $relationOptions, '1');

                    $reverseTypeOfRelationship = $this->choice(class_basename($relatedModel) . ' .... '
                        . class_basename($entityClass) . '(s)',
                        $relationOptions, '1');

                    $relationships[] = [
                        'entity' => $entityClass,
                        'related_entity' => $relatedModel,
                        'type' => $typeOfRelationship,
                        'reversed_type' => $reverseTypeOfRelationship,
                    ];

                    $this->info('Relationship successfully set!');

                    $question = 'Do you want to add another relationship? [y|N]';
                } else {
                    $this->error('The class "' . $relatedModel . '" does not exists');

                    $question = 'Do you want to try it again? [y|N]';
                }
            } else {
                break; // Stop while loop
            }
        }

        return $relationships;
    }

    /**
     * Parse the name and format according to the root namespace.
     *
     * @param  string $name
     * @return string
     */
    private function parseName($name)
    {
        $rootNamespace = $this->laravel->getNamespace();

        if (Str::startsWith($name, $rootNamespace)) {
            return $name;
        }

        if (Str::contains($name, '/')) {
            $name = str_replace('/', '\\', $name);
        }

        return $this->parseName(trim($rootNamespace, '\\') . '\\' . $name);
    }

    /**
     * Filter the properties, needed because Laracast/Generators gives 2 properties for a foreign key
     *
     * @param array $properties
     * @return array
     */
    private function filterProperties(array $properties)
    {
        $foreignKeys = [];
        foreach ($properties as $property) {
            if ($property['type'] == 'foreign') {
                $foreignKeys[] = $property['name'];
            }
        }

        // Remove the properties with the name of a foreign key but that not have a foreign type
        $filteredProperties = [];
        foreach ($properties as $property) {
            if (!in_array($property['name'], $foreignKeys) OR $property['type'] == 'foreign') {
                $filteredProperties[] = $property;
            }
        }

        return $filteredProperties;
    }
}