<?php

namespace Rodw\LaravelGenerators\Listeners;


use Illuminate\Support\Str;
use Rodw\FileManipulator\FileManipulatorInterface;
use Rodw\FileManipulator\Readers\ClassReaderInterface;
use Rodw\LaravelGenerators\Events\EntityWasCreated;
use Rodw\TemplateParser\TemplateParserInterface;

class SetPropertiesAsFillableInModel
{
    /**
     * @var FileManipulatorInterface
     */
    private $fileManipulator;

    /**
     * @var ClassReaderInterface
     */
    private $classReader;

    /**
     * @var TemplateParserInterface
     */
    private $templateParser;

    public function __construct(FileManipulatorInterface $fileManipulator, ClassReaderInterface $classReader,
                                TemplateParserInterface $templateParser)
    {
        $this->fileManipulator = $fileManipulator;
        $this->classReader = $classReader;
        $this->templateParser = $templateParser;
    }

    public function handle(EntityWasCreated $event)
    {
        $this->setFillableInCreatedModel($event);
        $this->updateOrSetFillableInRelatedModels($event);
    }

    private function updateOrSetFillableInRelatedModels(EntityWasCreated $event)
    {
        foreach ($event->getRelationships() as $relationship) {
            if ($relationship['type'] != '1' AND ($relationship['related_type'] == '1'
                    OR ($relationship['related_type'] == '0|1' AND $relationship['type'] != '0|1'))) {

                $className = $relationship['related_entity'];
                $classPath = $this->getPathOfClass($className);

                $foreignKey = Str::snake(class_basename($relationship['entity'])) . '_id';
                $fillableProperties = ['name' => $foreignKey];

                // Also add the already existing fillable array
                $entityModel = new $className();
                if ($entityModel) {
                    foreach ($entityModel->getFillable() as $property) {
                        $fillableProperties[] = ['name' => $property];
                    }
                }

                // Get the content to add
                $content = $this->getContentToAdd($fillableProperties);

                // Get the fillable property positions
                $this->classReader->read($classPath);
                $fillableStartPosition = $this->classReader->getPropertyStartPosition('fillable');
                $fillableEndPosition = $this->classReader->getPropertyEndPosition('fillable');

                // Replace the current fillable property with the new one
                $this->fileManipulator->setFile($classPath)
                    ->replace('/.*/s', $content, $fillableStartPosition, $fillableEndPosition)
                    ->save();
            }
        }
    }

    private function setFillableInCreatedModel(EntityWasCreated $event)
    {
        $classPath = $this->getPathOfClass($event->getEntityClass());

        // Read the class
        $this->classReader->read($classPath);

        // Add the fillable array
        $this->fileManipulator->setFile($classPath)
            ->add($this->getContentToAdd($event->getProperties()), $this->classReader->getLastPropertyLine())
            ->save();
    }

    private function getContentToAdd($properties)
    {
        return $this->templateParser->template(config('rodw_laravel_generators.templates.create_fillable'), [
            'properties' => $properties,
        ]);
    }

    private function getPathOfClass($className)
    {
        return base_path() . '/' . str_replace('\\', '/', $className) . '.php';
    }
}