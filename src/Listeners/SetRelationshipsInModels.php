<?php

namespace Rodw\LaravelGenerators\Listeners;


use Rodw\FileManipulator\FileManipulatorInterface;
use Rodw\FileManipulator\Readers\ClassReaderInterface;
use Rodw\LaravelGenerators\Events\EntityWasCreated;
use Rodw\LaravelGenerators\MakeEntity;
use Rodw\TemplateParser\TemplateParserInterface;

class SetRelationshipsInModels
{
    /**
     * @var FileManipulatorInterface
     */
    private $fileManipulator;

    /**
     * @var ClassReaderInterface
     */
    private $classReader;

    /**
     * @var TemplateParserInterface
     */
    private $templateParser;

    public function __construct(FileManipulatorInterface $fileManipulator, ClassReaderInterface $classReader,
                                TemplateParserInterface $templateParser)
    {
        $this->fileManipulator = $fileManipulator;
        $this->classReader = $classReader;
        $this->templateParser = $templateParser;
    }

    public function handle(EntityWasCreated $event)
    {
        foreach ($relationships = $event->getRelationships() as $relationship) {
            $this->addRelationshipInModel($relationship['entity'], $relationship['type'], $relationship['related_entity'], $relationship['reversed_type'], true);
            $this->addRelationshipInModel($relationship['related_entity'], $relationship['reversed_type'], $relationship['entity'], $relationship['type']);
        }

        if (!empty($relationships)) {
            return function(MakeEntity $makeEntity) {
                return $makeEntity->info('Relationships successfully set in the models!');
            };
        }

        return null;
    }

    /**
     * @param String $model - full class name of the model
     * @param String $type - one of the following: 1, 0|1, 0|*, 1|*
     * @param String $relatedModel - full class name of the related model
     * @param String $reversedType - one of the following: 1, 0|1, 0|*, 1|
     * @param bool $ownerOnEqual - if it's the owner when the relationship is equal e.g. 1 <-> 1
     */
    private function addRelationshipInModel($model, $type, $relatedModel, $reversedType, $ownerOnEqual = false)
    {
        // Use the right template file
        $eloquentType = $this->getEloquentType($type, $reversedType, $ownerOnEqual);
        $template = 'rodw_laravel_generators.templates.' . $eloquentType . '_relationship';

        // Get the content to add
        $content = $this->templateParser->template(config($template), [
            'entityName' => class_basename($model),
            'relatedModel' => $relatedModel,
        ]);

        $classPath = $this->getPathOfClass($model);
        $this->classReader->read($classPath);

        // Search for the line where the relationship should be put
        $addAfterLine = null;
        if ($this->classReader->getMethodStartLine('__construct')) {
            $addAfterLine = $this->classReader->getMethodEndLine('__construct');
        } else {
            $addAfterLine = $this->classReader->getLastPropertyLine();
        }

        // Add the new content in the model and save it
        $this->fileManipulator->setFile($classPath)
            ->add($content, $addAfterLine)
            ->save();
    }

    private function getPathOfClass($className)
    {
        return base_path() . '/' . str_replace('\\', '/', $className) . '.php';
    }

    /**
     * @param $type
     * @param $reversedType
     * @param bool $ownerOnEqual - if it's the owner when the relationship is equal e.g. 1 <-> 1
     * @return string
     */
    private function getEloquentType($type, $reversedType, $ownerOnEqual = false)
    {
        switch ($type) {
            case '1':
                if ($reversedType != '1' OR $ownerOnEqual) {
                    $eloquentType = 'belongs_to';
                } else {
                    $eloquentType = 'has_one';
                }
                break;
            case '0|1':
                if ($reversedType == '1' OR ($reversedType == '0|1' AND !$ownerOnEqual)) {
                    $eloquentType = 'has_one';
                } else {
                    $eloquentType = 'belongs_to';
                }
                break;
            case '0|*':
            case '1|*':
                if ($reversedType == '1' OR $reversedType == '0|1') {
                    $eloquentType = 'has_many';
                } else {
                    $eloquentType = 'belongs_to_many';
                }
                break;
            default:
                throw new \InvalidArgumentException('Type can not be "' . $type . '", but should be one the following: 1, 0|1, 0|*, 1|*');
                break;
        }

        return $eloquentType;
    }
}