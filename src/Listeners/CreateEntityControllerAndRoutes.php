<?php

namespace Rodw\LaravelGenerators\Listeners;


use Illuminate\Support\Str;
use Rodw\FileManipulator\Readers\ClassReaderInterface;
use Rodw\FileManipulator\Readers\FileReaderInterface;
use Rodw\LaravelGenerators\Events\EntityWasCreated;
use Rodw\FileManipulator\FileManipulatorInterface;
use Rodw\Generator\GeneratorInterface;
use Rodw\LaravelGenerators\MakeEntity;
use Rodw\TemplateParser\TemplateParserInterface;

class CreateEntityControllerAndRoutes
{
    /**
     * @var GeneratorInterface
     */
    private $generator;

    /**
     * @var FileManipulatorInterface
     */
    private $fileManipulator;

    /**
     * @var TemplateParserInterface
     */
    private $templateParser;

    /**
     * @var ClassReaderInterface
     */
    private $classReader;

    /**
     * @var FileReaderInterface
     */
    private $fileReader;

    /**
     * Create the event listener.
     *
     * @param GeneratorInterface $generator
     * @param FileManipulatorInterface $fileManipulator
     * @param TemplateParserInterface $templateParser
     * @param ClassReaderInterface $classReader
     * @param FileReaderInterface $fileReader
     */
    public function __construct(GeneratorInterface $generator, FileManipulatorInterface $fileManipulator,
                                TemplateParserInterface $templateParser, ClassReaderInterface $classReader,
                                FileReaderInterface $fileReader)
    {
        $this->generator = $generator;
        $this->fileManipulator = $fileManipulator;
        $this->templateParser = $templateParser;
        $this->classReader = $classReader;
        $this->fileReader = $fileReader;
    }

    /**
     * Handle the event.
     *
     * @param  EntityWasCreated  $event
     * @return String
     */
    public function handle(EntityWasCreated $event)
    {
        $this->createRoutes($event);
        $this->createController($event);
        $this->createValidationRequest($event);
        $this->updateValidationRequest($event);

        $this->addValidationRulesForRelationships($event);

        $this->createModelFactory($event);
        $this->updateModelFactoriesFromRelationships($event);
        $this->createControllerTests($event);

        return function(MakeEntity $makeEntity) {
            return $makeEntity->info('Controller, routes and validation requests created successfully!');
        };
    }

    /**
     * @param EntityWasCreated $event
     */
    private function createRoutes(EntityWasCreated $event)
    {
        $entityName = $event->getEntityName();

        $routeToAdd = $this->templateParser->template(config('rodw_laravel_generators.templates.add_route'), [
            'entity' => $entityName,
        ]);

        $this->fileManipulator->setFile(config('rodw_laravel_generators.paths.routes'))
            ->add($routeToAdd)
            ->save();
    }

    /**
     * @param EntityWasCreated $event
     */
    private function createController(EntityWasCreated $event)
    {
        $entityClass = $event->getEntityClass();
        $entityName = $event->getEntityName();

        $this->generator->generate(config('rodw_laravel_generators.paths.controllers') . '/' . $entityName . 'Controller.php',
            config('rodw_laravel_generators.templates.entity_controller'), [
            'entityName' => $entityName,
            'entityClass' => $entityClass,
        ]);
    }

    private function createValidationRequest(EntityWasCreated $event)
    {
        $entityName = $event->getEntityName();

        $this->generator->generate($this->getCreateValidationRequestPath($entityName),
            config('rodw_laravel_generators.templates.create_validation_request'), [
                'entity' => $entityName,
                'properties' => $event->getProperties(),
            ]);
    }

    private function updateValidationRequest(EntityWasCreated $event)
    {
        $entityName = $event->getEntityName();

        $this->generator->generate($this->getUpdateValidationRequestPath($entityName),
            config('rodw_laravel_generators.templates.update_validation_request'), [
                'entity' => $entityName,
                'properties' => $event->getProperties(),
            ]);
    }

    private function addValidationRulesForRelationships(EntityWasCreated $event)
    {
        $entityName = $event->getEntityName();
        $entitySnake = Str::snake($entityName);
        $table = Str::plural($entitySnake);
        $column = $entitySnake . '_id';

        foreach ($event->getRelationships() as $relationship) {
            $relationshipSnake = Str::snake(class_basename($relationship['related_entity']));
            $relationshipTable = Str::plural($relationshipSnake);
            $relationshipColumn = $relationshipSnake . '_id';

            $this->addRelationshipRules($entityName, $relationship['type'], $relationshipTable, $relationshipColumn);
            $this->addRelationshipRules($relationship['related_entity'], $relationship['reversed_type'], $table, $column);
        }
    }

    private function addRelationshipRules($entityName, $type, $tableOfRelatedEntity, $foreignKeyColumn)
    {
        // Get the class paths for the entity
        $entityName = class_basename($entityName);
        $createPath = $this->getCreateValidationRequestPath($entityName);
        $updatePath = $this->getUpdateValidationRequestPath($entityName);

        $rules = '';
        $foreignKeyRule = '|integer|min:1|exists:' . $tableOfRelatedEntity . ',id';

        if ($type == '1' OR $type == '1|*') {
            // This types are required
            $rules .= '|required';
        }

        if ($type == '1|*' OR $type == '0|*') {
            // The array should contain unique values
            $rules .= '|distinct';
            // The column is an array
            $foreignKeyColumn .= '.*';

            if ($type == '1|*') {
                $firstIndexColumn = $foreignKeyColumn . '.0';
                $firstIndexRules = 'required' . $foreignKeyRule;

                $this->addRulesInFile($createPath, $firstIndexColumn, $firstIndexRules);
                $this->addRulesInFile($updatePath, $firstIndexColumn, $firstIndexRules);
            }
        }

        $rules .= $foreignKeyRule;
        $rules = ltrim($rules, '|');

        // Add the rules in the create validation file
        $this->addRulesInFile($createPath, $foreignKeyColumn, $rules);

        // Also add new rules to the update validation
        $this->addRulesInFile($updatePath, $foreignKeyColumn, $rules);
    }

    /**
     * Method that can add validation rules to an existing validation (request) file
     *
     * @param $filePath
     * @param $column
     * @param $rules
     */
    private function addRulesInFile($filePath, $column, $rules)
    {
        if (file_exists($filePath)) {
            // Add validation rules for the relationship
            $this->classReader->read($filePath);

            // Search for the array start
            $rulesStartPosition = $this->classReader->getMethodStartPosition('rules');
            $rulesEndPosition = $this->classReader->getMethodEndPosition('rules');

            // Regex patterns
            $pattern1 = '/(return[\s]+\[)/i';
            $pattern2 = '/(return[\s]+array[\s]*\()/i';

            $this->fileReader->read($filePath);
            $arrayStartPosition1 = $this->fileReader->getFirstPositionFor($pattern1, $rulesStartPosition,
                $rulesEndPosition, true);

            $arrayStartPosition2 = $this->fileReader->getFirstPositionFor($pattern2, $rulesStartPosition,
                $rulesEndPosition, true);

            // We can only add something if we can find the start position of the array
            if (!is_null($arrayStartPosition1) OR !is_null($arrayStartPosition2)) {
                $this->fileManipulator->setFile($filePath);

                $replacement = "$1\r\n\t\t\t'" . $column . "' => '" . $rules . "',";

                if (is_null($arrayStartPosition1) OR (!is_null($arrayStartPosition2) AND $arrayStartPosition2 < $arrayStartPosition1)) {
                    $this->fileManipulator->replace($pattern2, $replacement, $rulesStartPosition, $rulesEndPosition);
                } else {
                    $this->fileManipulator->replace($pattern1, $replacement, $rulesStartPosition, $rulesEndPosition);
                }

                $this->fileManipulator->save();
            }
        }
    }

    private function createModelFactory(EntityWasCreated $event)
    {
        $entityName = $event->getEntityName();
        $entityClass = $event->getEntityClass();

        $foreignKeyClassMap = [];
        foreach ($event->getRelationships() as $relationship) {
            if ($relationship['type'] == '1' OR ($relationship['type'] == '0|1' AND $relationship['reversed_type'] != '1')) {
                $foreignKey = Str::snake(class_basename($relationship['related_entity'])) . '_id';
                $foreignKeyClassMap[$foreignKey] = $relationship['related_entity'];
            }
        }

        $this->generator->generate($this->getModelFactoryPath($entityName),
            config('rodw_laravel_generators.templates.model_factory'), [
                'entityClass' => $entityClass,
                'properties' => $event->getProperties(),
                'foreignKeyClassMap' => $foreignKeyClassMap,
            ]);
    }

    private function updateModelFactoriesFromRelationships(EntityWasCreated $event)
    {
        $entityName = $event->getEntityName();
        
        foreach ($event->getRelationships() as $relationship) {
            if ($relationship['type'] != '1' AND ($relationship['reversed_type'] == '1' OR ($relationship['type'] != '0|1' AND $relationship['reversed_type'] == '0|1'))) {
                $relatedEntity = $relationship['related_entity'];

                // Path to the ModelFactory
                $path = $this->getModelFactoryPath($relatedEntity);
                // We can only add something if the file exists
                if (file_exists($path)) {
                    $basePattern = '/\$factory[\s]*->[\s]*define[\s]*\([\s]*' . $relatedEntity . '::class[\s]*,[\s]*function[\s]*\([\s]*Faker\\Generator[\s]*\$faker[\s]*\)[\s]*{[\s]*return[\s]*(';
                    $pattern1 = $basePattern . '\[)/i';
                    $pattern2 = $basePattern . 'array[\s]*\()/i';

                    $this->fileReader->read($path);
                    $arrayStartPosition1 = $this->fileReader->getFirstPositionFor($pattern1, null, null, true);
                    $arrayStartPosition2 = $this->fileReader->getFirstPositionFor($pattern2, null, null, true);

                    // We can only add something if we can find the start position of the array
                    if (!is_null($arrayStartPosition1) OR !is_null($arrayStartPosition2)) {
                        $this->fileManipulator->setFile($path);

                        $replacement = $this->templateParser->template(config('rodw_laravel_generators.templatesadd_foreign_key_to_model_factory'), [
                            'relatedClass' => $relationship['entity'],
                            'foreignKey' => Str::snake($entityName) . '_id',
                        ]);
                        $replacement = '$1' . $replacement;

                        if (is_null($arrayStartPosition1) OR (!is_null($arrayStartPosition2) AND $arrayStartPosition2 < $arrayStartPosition1)) {
                            $this->fileManipulator->replace($pattern2, $replacement);
                        } else {
                            $this->fileManipulator->replace($pattern1, $replacement);
                        }

                        $this->fileManipulator->save();
                    }
                }
            }
        }
    }

    private function createControllerTests(EntityWasCreated $event)
    {
        $entityName = $event->getEntityName();
        $entityClass = $event->getEntityClass();

        $this->generator->generate($this->getControllerTestPath($entityName),
            config('rodw_laravel_generators.templates.controller_test'), [
                'entityName' => $entityName,
                'entityClass' => $entityClass,
                'properties' => $event->getProperties(),
            ]);
    }

    private function getCreateValidationRequestPath($entityName)
    {
        return config('rodw_laravel_generators.paths.requests') . '/Create' . $entityName . 'Request.php';
    }

    private function getUpdateValidationRequestPath($entityName)
    {
        return config('rodw_laravel_generators.paths.requests') . '/Update' . $entityName . 'Request.php';
    }

    private function getControllerTestPath($entityName)
    {
        return config('rodw_laravel_generators.paths.controller_tests') . '/' . $entityName . 'Test.php';
    }

    private function getModelFactoryPath($entityName)
    {
        return config('rodw_laravel_generators.paths.model_factories') . '/' . $entityName . 'Factory.php';
    }
}